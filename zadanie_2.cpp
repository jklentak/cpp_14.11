#include <iostream>

using namespace std;

int main() {
    int a, b;
    cout << "Wprowadź liczba A: ";
    cin >> a;
    cout  << endl << "Wprowadź liczba B: ";
    cin >> b;

    if(a<0 || b<0){
        cout << "Błędnie wprowadzone liczby" <<endl;
    }else{
//        Suma
        if(a+b>0){
            cout << "Suma a, b jest dodatnia" <<endl;
        }else{
            cout << "Suma a, b jest równa 0" <<endl;
        }
//        Parzyste
        if((a+b)%2==0){
            cout << "Suma a, b jest parzysta." <<endl;
        }else{
            cout << "Suma a, b nie jest parzysta." <<endl;
        }
    }

    return 0;
}
