#include <iostream>

using namespace std;

int main(){
int wiek;

cout << "Wprowadź wiek: ";
cin >> wiek;

if(wiek < 1 || wiek > 100){
  cout << "Wprowadzono nieprawidłowy wiek \n";
}else{
  if(wiek < 18){
    cout << "dziecko \n";
  }else if(wiek >=18 && wiek <50){
    cout << "osoba dorosla \n";
  }else{
    cout << "osoba starsza \n";
  }
}
return 0;
}
