#include <iostream>

using namespace std;

int main(){
  int a, b, c;
  cout << "Wprowadź 3 katy trójkąta \n";
  cout << "Bok a: ";
  cin >> a;
  cout << "Bok b: ";
  cin >> b;
  cout << "Bok c: ";
  cin >> c;

  if(a<=0 || b<=0 || c<=0){
    cout << "Wprowadzono nieprawidlowe dane \n";
  }else{
    if(a+b+c==180){
      cout << "Z podanych katow mozna stworzyc trojkat ";
      if(a>90 || b>90 || c>90){
        cout << "rozwartokątny \n";
      }else if(a<90 && b<90 && c<90){
        cout << "ostrokatny \n";
      }else{
        cout << "prostokątny \n";
      }
    }else{
      cout << "Nie można zbudowac trojkata z podanych katow \n";
    }
  }

  return 0;
}
