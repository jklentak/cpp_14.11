#include <iostream>

using namespace std;

int main(){
  int a, b, c;
  cout << "Wprowadź 3 boki trójkąta \n";
  cout << "Bok a: ";
  cin >> a;
  cout << "Bok b: ";
  cin >> b;
  cout << "Bok c: ";
  cin >> c;

  // Sprawdzenie czy boki >0
  if(a>0 || b>0 || c>0){
    if(a+b>c && a+c>b && b+c>a){
      cout << "Z podanych bokow mozna stworzyc trojkat ";
      if(a==b && a==c){
        cout << "rownoboczny \n";
      }else if(a==b || a==c || b==c){
        cout << "rownoramienny \n";
      }else if((a*a)+(b*b)==(c*c) || (a*a)+(c*c)==(b*b) || (b*b)+(c*c)==(a*a)){
        cout << "prostokątny \n";
      }
    }else{
      cout << "Z podanych bokow NIE mozna stworzyc trojkata. \n";
    }
  }else{
    cout << "Wprowadzono zle dane. \n";
  }

return 0;
}
