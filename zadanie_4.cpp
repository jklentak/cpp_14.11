#include <iostream>
#include <math.h>

using namespace std;

int main(){
double a,b;
cout << "Wprowadz dwie liczby dodatnie i parzyste. \n";
cout << "Wprowadz a: ";
cin >> a;
cout << "Wprowadz b: ";
cin >> b;


// Nieprawidlowe dane
if(a<=0 || b<=0 || a%2!=0 || b%2!=0){
  cout << "Wprowadzono złe dane! \n";
}else{
  cout << "Wprowadzono poprawne dane. \n";
  if(a>=10 && a<=100 && b>=10 && b<=100){
    cout << "Suma liczb = " << a+b <<endl;
  }else{
    cout << "Liczby nie należą do przedziału <10;100> \n";
  }
}

return 0;
}
